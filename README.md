# vue-blog

## 技术点
用到了 vue-cli vue-router vuex element-ui leanCloud
实现了 登录 注册 发表和修改文章, 关注用户, 发送私信,查看朋友圈等社交功能


## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
